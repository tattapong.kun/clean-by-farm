package delivery

import (
	"clean-architecture-go/domain"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

// Handler ..
type Handler struct {
	usecase domain.ShopUsecase
}

// NewHandler ..
func NewHandler(e *echo.Group, shopUsecase domain.ShopUsecase) *Handler {
	handler := Handler{usecase: shopUsecase}

	e.GET("/shops", handler.List)

	return &handler
}

// List ..
func (handler *Handler) List(c echo.Context) error {
	result, err := handler.usecase.ListShop()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"data": result})
}

func (h *Handler) Get(c echo.Context) error {
	shopID, err := strconv.Atoi(c.Param("shop_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
	}

	result, err := h.usecase.GetShop(shopID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"data": result, "error": nil})
}

func (h *Handler) Update(c echo.Context) error {
	shop := domain.Shop{}
	if bindErr := c.Bind(&shop); bindErr != nil {
		return c.JSON(http.StatusOK, map[string]interface{}{"error": bindErr.Error()})
	}

	err := h.usecase.UpdateShop(shop)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"error": nil})
}

func (h *Handler) Create(c echo.Context) error {
	shop := domain.Shop{}
	if bindErr := c.Bind(&shop); bindErr != nil {
		return c.JSON(http.StatusOK, map[string]interface{}{"error": bindErr.Error()})
	}

	result, err := h.usecase.CreateShop(shop)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"data": result, "error": nil})
}

func (h *Handler) Delete(c echo.Context) error {
	shopID, err := strconv.Atoi(c.Param("shop_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
	}

	queryErr := h.usecase.DeleteShop(shopID)
	if queryErr != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": queryErr.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"error": nil})
}

func (h *Handler) UnDelete(c echo.Context) error {
	shopID, err := strconv.Atoi(c.Param("shop_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
	}

	queryErr := h.usecase.UndeleteShop(shopID)
	if queryErr != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": queryErr.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"error": nil})
}
