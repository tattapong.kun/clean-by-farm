package repository

import (
	"clean-architecture-go/domain"
	"fmt"

	"gorm.io/gorm"
)

type shopRepository struct {
	db *gorm.DB
}

func NewShopRepository(db *gorm.DB) domain.ShopRepository {
	return &shopRepository{
		db: db,
	}
}

func (shopRepo *shopRepository) List() ([]domain.Shop, error) {
	shops := []domain.Shop{}

	if getErr := shopRepo.db.Find(&shops).Error; getErr != nil {

		return shops, fmt.Errorf("Get All Shops error %s", getErr.Error())
	}

	return shops, nil
}

func (shopRepo *shopRepository) Get(shopID int) (domain.Shop, error) {
	shop := domain.Shop{}

	if getErr := shopRepo.db.Where("shop_id = ?", shopID).Scan(&shop).Error; getErr != nil {

		return shop, fmt.Errorf("Get Shop By ID error %s", getErr.Error())
	}

	return shop, nil
}

func (shopRepo *shopRepository) Create(shop domain.Shop) (domain.Shop, error) {
	if createErr := shopRepo.db.Create(&shop).Error; createErr != nil {
		nilShop := domain.Shop{}
		return nilShop, fmt.Errorf("Create Shop error %s", createErr.Error())
	}

	return shop, nil
}

func (shopRepo *shopRepository) Update(shop domain.Shop) error {
	if updateErr := shopRepo.db.Save(&shop).Error; updateErr != nil {
		return fmt.Errorf("Update Shop error %s", updateErr.Error())
	}
	return nil
}

func (shopRepo *shopRepository) Delete(shopID int) error {
	shopModel := domain.Shop{}
	if deleteErr := shopRepo.db.Where("shop_id = ?", shopID).Delete(&shopModel).Error; deleteErr != nil {
		return fmt.Errorf("Delete Shop error %s", deleteErr.Error())
	}
	return nil
}

func (shopRepo *shopRepository) Undelete(shopID int) error {
	if unDeleteErr := shopRepo.db.Where("id = ?", shopID).
		Updates(map[string]interface{}{
			"deleted_at": gorm.Expr("null"),
		}).Error; unDeleteErr != nil {
		return fmt.Errorf("Undelete Shop error %s", unDeleteErr.Error())
	}

	return nil
}
