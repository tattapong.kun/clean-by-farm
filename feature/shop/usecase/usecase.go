package usecase

import (
	"clean-architecture-go/domain"
)

type shopUsecase struct {
	repo domain.ShopRepository
}

func NewShopUsecase(repo domain.ShopRepository) domain.ShopUsecase {
	return &shopUsecase{
		repo: repo,
	}
}

func (usecase *shopUsecase) ListShop() ([]domain.Shop, error) {
	return usecase.repo.List()
}

func (usecase *shopUsecase) GetShop(shopID int) (domain.Shop, error) {
	return usecase.repo.Get(shopID)
}

func (usecase *shopUsecase) CreateShop(shop domain.Shop) (domain.Shop, error) {
	return usecase.repo.Create(shop)
}

func (usecase *shopUsecase) UpdateShop(shop *domain.Shop) error {
	return usecase.repo.Update(shop)
}

func (usecase *shopUsecase) DeleteShop(shopID int) error {
	return usecase.repo.Delete(shopID)
}

func (usecase *shopUsecase) UndeleteShop(shopID int) error {
	return usecase.repo.Undelete(shopID)
}
