package repository

import (
	"clean-architecture-go/domain"
	"fmt"

	"gorm.io/gorm"
)

type userRepository struct {
	db *gorm.DB
}

// NewUserRepository ..
func NewUserRepository(db *gorm.DB) domain.UserRepository {
	return &userRepository{
		db: db,
	}
}

func (userRepo *userRepository) List() ([]domain.User, error) {
	users := []domain.User{}

	if getAllErr := userRepo.db.Find(&users).Error; getAllErr != nil {
		return users, fmt.Errorf("Get All Users error %s", getAllErr.Error())
	}

	return users, nil
}

func (userRepo *userRepository) Get(userID int) (domain.User, error) {
	user := domain.User{}

	if getErr := userRepo.db.Where("user_id", userID).Scan(&user).Error; getErr != nil {
		return user, fmt.Errorf("Get User error %s", getErr.Error())
	}

	return user, nil
}

type getWithShop struct {
	ID    int     `json:"id"`
	Name  string  `json:"name"`
	Email string  `json:"email"`
	Shops *string `json:"shop"`
}

func (userRepo *userRepository) GetWithShop(userID int) (domain.UserWithShop, error) {
	//shops := []domain.Shop{}
	userWithShop := domain.UserWithShop{}

	// if getShopsByUserIDErr := userRepo.db.Where("id = ?", userID).
	// 	Joins("LEFT JOIN shops ON shops.user_id = shops.user_id").
	// 	Where("users.id = ?", userID).Group("users.id").Order("users.id").
	// 	Scan().Error; getShopsByUserIDErr != nil {

	// }

	return userWithShop, nil
}

func (userRepo *userRepository) Create(user domain.User) (domain.User, error) {
	if updateErr := userRepo.db.Save(&user).Error; updateErr != nil {
		nilUser := domain.User{}
		return nilUser, fmt.Errorf("Create User error %s", updateErr.Error())
	}

	return user, nil
}

func (userRepo *userRepository) Update(user domain.User) error {
	if updateErr := userRepo.db.Save(&user).Error; updateErr != nil {

		return fmt.Errorf("Update User error %s", updateErr.Error())
	}
	return nil
}

func (userRepo *userRepository) Delete(userID int) error {
	userModel := domain.Shop{}
	if deleteErr := userRepo.db.Where("id = ?", userID).Delete(&userModel).Error; deleteErr != nil {
		return fmt.Errorf("Delete User error %s", deleteErr.Error())
	}
	return nil
}

func (userRepo *userRepository) Undelete(userID int) error {
	if unDeleteErr := userRepo.db.Where("id = ?", userID).
		Updates(map[string]interface{}{
			"deleted_at": gorm.Expr("null"),
		}).Error; unDeleteErr != nil {
		return fmt.Errorf("Undelete User error %s", unDeleteErr.Error())
	}
	return nil
}
