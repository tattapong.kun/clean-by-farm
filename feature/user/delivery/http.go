package delivery

import (
	"clean-architecture-go/domain"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

// Handler ..
type Handler struct {
	usecase domain.UserUsecase
}

type UserWithShopHandler struct {
	userUseCase domain.UserWithShop
}

// NewHandler ..
func NewHandler(e *echo.Group, u domain.UserUsecase) *Handler {
	h := Handler{usecase: u}

	e.GET("/users", h.List)

	return &h
}

// List ..
func (h *Handler) List(c echo.Context) error {
	result, err := h.usecase.ListUser()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"data": result})
}

func (h *Handler) Get(c echo.Context) error {
	userID, err := strconv.Atoi(c.Param("user_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
	}

	result, err := h.usecase.GetUser(userID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"data": result})
}

func (h *Handler) GetWithShop(c echo.Context) error {
	userID, err := strconv.Atoi(c.Param("user_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
	}

	result, err := h.usecase.GetUserWithShop(userID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"data": result})
}

func (h *Handler) Update(c echo.Context) error {
	user := domain.User{}
	if bindErr := c.Bind(&user); bindErr != nil {
		return c.JSON(http.StatusOK, map[string]interface{}{"error": bindErr.Error()})
	}

	err := h.usecase.UpdateUser(user)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"error": nil})
}

func (h *Handler) Create(c echo.Context) error {
	user := domain.User{}
	if bindErr := c.Bind(&user); bindErr != nil {
		return c.JSON(http.StatusOK, map[string]interface{}{"error": bindErr.Error()})
	}

	result, err := h.usecase.CreateUser(user)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"data": result, "error": nil})
}

func (h *Handler) Delete(c echo.Context) error {
	userID, err := strconv.Atoi(c.Param("user_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
	}

	queryErr := h.usecase.DeleteUser(userID)
	if queryErr != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": queryErr.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"error": nil})
}

func (h *Handler) UnDelete(c echo.Context) error {
	userID, err := strconv.Atoi(c.Param("user_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
	}

	queryErr := h.usecase.UndeleteUser(userID)
	if queryErr != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": queryErr.Error()})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"error": nil})
}
