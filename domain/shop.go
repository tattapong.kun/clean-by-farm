package domain

import "time"

// Shop ..
type Shop struct {
	ID        int        `json:"shop_id"`
	Name      string     `json:"name"`
	UserID    string     `json:"user_id"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:now();"`
	DeletedAt *time.Time `json:"deleted_at"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:now();"`
}

// ShopUsecase ..
type ShopUsecase interface {
	ListShop() ([]Shop, error)
	GetShop(shopID int) (Shop, error)
	//GetShopWithUser(userID int) (UserWithShop, error)
	CreateShop(Shop) (Shop, error)
	UpdateShop(*Shop) error
	DeleteShop(shopID int) error
	UndeleteShop(shopID int) error
}

// ShopRepository ..
type ShopRepository interface {
	List() ([]Shop, error)
	Get(shopID int) (Shop, error)
	//GetWithShop(shopID int) (UserWithShop, error)
	Create(Shop) (Shop, error)
	Update(Shop) error
	Delete(shopID int) error
	Undelete(shopID int) error
}
